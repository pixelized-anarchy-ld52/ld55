extends Camera3D

var y_plane

func _ready():
	set_as_top_level(true)
	y_plane = global_position.y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	global_position = Vector3(owner.global_position.x, y_plane, owner.global_position.z)
	global_rotation = Vector3(global_rotation.x, owner.global_rotation.y - PI/2, global_rotation.z)
