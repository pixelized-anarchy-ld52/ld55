extends Node3D

@onready var cams = {
	cam_top  = $Cam_Top,
	cam_side = $Cam_Side,
	cam_fp   = $Cam_Fp,
	cam_tp   = $Cam_Tp
}

func _input(event):
	for cam in cams:
		if event.is_action_pressed(cam):
			cams[cam].make_current()
