extends Camera3D

@export_range(0, 1, 0.01) var y_angular_speed := 0.1
var y_angle := 0.

@export var mouselook_sensitivity := 0.001
var mouselook := Vector2.ZERO
var prev_current := false

@onready var player: Node3D = owner # a raíz do .tscn
@onready var offset_from_player: Vector3 = position - player.global_position


func _init() -> void:
	# deserda os transforms dos ancestors porque esta camera quer desenrascar-se sozinha
	# (then `position` no longer sucks, because it's == global_position)
	top_level = true


func _process(_delta: float) -> void:
	y_angle = lerp_angle(y_angle, player.global_rotation.y, y_angular_speed)
	var y_look := y_angle - mouselook.x

	position = (
		player.global_position +
		offset_from_player
			.rotated(Vector3.FORWARD, mouselook.y)
			.rotated(Vector3.UP, y_look)
	)

	var invisible_wall := Plane(
		player.global_transform.basis.rotated(Vector3.UP, -mouselook.x - PI/2) * Vector3.FORWARD,
		player.global_transform.origin
	)
	if invisible_wall.is_point_over(position):
		position = invisible_wall.project(position)

	#DebugDraw3D.draw_plane(invisible_wall)
	#DebugDraw3D.draw_arrow_ray(player.global_position + 2.5 * Vector3.UP, invisible_wall, 2.0)

	var position_y = clampf(position.y, player.global_position.y + near, player.global_position.y + 20.0)
	position_y += 3 * smoothstep(1.2, 0, -invisible_wall.distance_to(position))
	position = Vector3(position.x, position_y, position.z)
	position -= 2 * invisible_wall.normal * smoothstep(1.5, 0.5, -invisible_wall.distance_to(position))

	rotation = Vector3(-0.5 * mouselook.y, y_look - PI/2, rotation.z)

	if not current and prev_current:
		mouselook = Vector2.ZERO
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	elif current and not prev_current:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

	prev_current = current


func _input(event: InputEvent) -> void:
	var motion_event := event as InputEventMouseMotion
	if motion_event and current:
		mouselook += motion_event.relative * mouselook_sensitivity
		mouselook = mouselook.clamp(Vector2(-INF, -PI/2), Vector2(INF, PI/2))
		print("%.3v" % mouselook)
