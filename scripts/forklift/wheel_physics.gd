extends RayCast3D

@onready var car: RigidBody3D = $".."
enum Axle {FRONT, REAR}
@export var axle: Axle

@onready var wheel := get_child(0) as Node3D
@onready var wheel_initial_pos := wheel.position
var wheel_rot_vel: float
var wheel_spring_offset: float

func _process(delta):
	#rotacao rodas tem bug pq o carro ta sempre sliding
	# wheel_rot_vel
	var wheel_radius = car.f_wheel_radius if axle == Axle.FRONT else car.r_wheel_radius
	wheel.rotate_z(-wheel_rot_vel / (2*PI*wheel_radius) * 0.06)
	wheel_rot_vel *= 1 - .2 * delta

	wheel.global_position = global_position + global_basis * wheel_initial_pos
	wheel.global_position += wheel_spring_offset * global_basis.y

func _physics_process(delta):

	if axle == Axle.REAR:
		var speed_adjusted_hardness = car.steering_hardness / clampf(0.2 * car.linear_velocity.length(), 1, 4)  # TODO this could be a curve
		if Input.is_action_pressed("left"):
			rotation.y = move_toward(rotation.y, -car.steering_range, speed_adjusted_hardness)
		elif Input.is_action_pressed("right"):
			rotation.y = move_toward(rotation.y, car.steering_range, speed_adjusted_hardness)
		else:
			rotation.y = move_toward(rotation.y, 0, speed_adjusted_hardness)

	if is_colliding():
		# Use this instead of `position`
		var wheel_offset = global_position - car.global_position
		# GetPointVelocity()
		var wheel_velocity = car.linear_velocity + car.angular_velocity.cross(wheel_offset)

		if axle == Axle.FRONT:
			if Input.is_action_pressed("forward"):
				car.apply_force(global_basis * Vector3(car.engine_power,0,0), wheel_offset)
			if Input.is_action_pressed("back"):
				car.apply_force(global_basis * Vector3(-car.engine_power,0,0), wheel_offset)


		# Fetch values from car configuration
		var rest_distance = car.f_rest_distance     if axle == Axle.FRONT else car.r_rest_distance
		var spring_strength = car.f_spring_strength if axle == Axle.FRONT else car.r_spring_strength
		var damper_strength = car.f_damper_strength if axle == Axle.FRONT else car.r_damper_strength
		var grip_factor = car.f_grip_factor         if axle == Axle.FRONT else car.r_grip_factor
		var wheel_mass = car.f_wheel_mass           if axle == Axle.FRONT else car.r_wheel_mass

		# Y-axis: Suspension
		var spring_direction = global_basis.y
		wheel_spring_offset = rest_distance - (get_collision_point().distance_to(global_position))
		var vel = spring_direction.dot(wheel_velocity)
		var spring_magnitude = (wheel_spring_offset * spring_strength) - (vel * damper_strength)
		var spring_force = spring_magnitude * spring_direction
		car.apply_force(spring_force, wheel_offset)

		# X-axis: acceleration and braking
		var friction: Vector3 = (wheel_velocity.dot(global_basis.x) * global_basis.x) * -0.1
		car.apply_force(friction, wheel_offset)

		# Z-axis: steering
		var wheel_direction_perp: Vector3 = global_basis.z.normalized()
		var car_slip: float = wheel_velocity.dot(wheel_direction_perp)
		var speed_adjusted_grip_factor = grip_factor / clampf(1.2 * abs(car_slip), 1, 10)  # TODO this could be a Curve
		var desired_vel_change = -grip_factor * car_slip
		var desired_accel = desired_vel_change / delta
		var anti_slip_mag = wheel_mass * desired_accel  # a força é má
		var anti_slip_force: Vector3 = wheel_direction_perp * anti_slip_mag
		car.apply_force(anti_slip_force, wheel_offset)

		# wheel motion
		wheel_rot_vel = wheel_velocity.dot(global_basis.x.normalized())

		# debug

		#DebugDraw3D.draw_arrow_ray(car.global_position + 4 * Vector3.UP, car.linear_velocity.normalized(), car.linear_velocity.length(), Color("88AABB", .25), 0.5, true)
		DebugDraw3D.draw_ray(global_position, global_basis.x, 20, Color.BLACK)
		DebugDraw3D.draw_ray(global_position, wheel_direction_perp, 20, Color.BLACK)
		DebugDraw3D.draw_ray(global_position, wheel_velocity.normalized(), wheel_velocity.length(), Color("aaeebb"))
		DebugDraw3D.draw_arrow_ray(global_position, wheel_direction_perp, car_slip, Color.ORANGE_RED, 0.4, true)
		DebugDraw3D.draw_arrow_ray(global_position, anti_slip_force.normalized(), anti_slip_force.length(), Color.BLUE,  0.2, true)
		DebugDraw3D.draw_arrow_ray(global_position, spring_force.normalized(), 0.5 * spring_force.length(), Color.SKY_BLUE,  0.2, true)
		DebugDraw3D.draw_arrow_ray(global_position, friction.normalized(), friction.length(), Color.GRAY,  0.2, true)

		DebugDraw2D.config.text_block_position = DebugDraw2DConfig.POSITION_LEFT_BOTTOM
		DebugDraw2D.begin_text_group(name)
		DebugDraw2D.set_text("point velocity", "|%.3v| = %.3f" % [wheel_velocity, wheel_velocity.length()])
		DebugDraw2D.set_text("tire slip", "%.3f" % car_slip)
		DebugDraw2D.set_text("tire grip", "%.3f" % speed_adjusted_grip_factor)
		DebugDraw2D.end_text_group()

