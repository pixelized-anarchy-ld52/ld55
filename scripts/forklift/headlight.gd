extends SpotLight3D

var headlights := true

func toggle_headlights() -> void:
	headlights = not headlights
	var final_val := 10.0 if headlights else 0.0
	create_tween() \
	  .tween_property(self, "light_energy", final_val, 0.5) \
	  .set_ease(Tween.EASE_OUT if headlights else Tween.EASE_IN) \
	  .set_trans(Tween.TRANS_BOUNCE)

func _ready() -> void:
	toggle_headlights()

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("headlights"):
		toggle_headlights()
