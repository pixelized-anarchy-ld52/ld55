extends RigidBody3D

@export var engine_power: float = 3.0

@export_group("Suspensions")
@export_subgroup("Front", "f_")
@export var f_rest_distance: float = 0.85;
@export var f_spring_strength: float = 100;
@export var f_damper_strength: float = 5;
@export var f_wheel_radius: float = 0.4;
@export var f_wheel_mass: float = 0.02;
@export_range(0, 1, 0.05) var f_grip_factor: float = 0.8

@export_subgroup("Rear", "r_")
@export var r_rest_distance: float = 0.85;
@export var r_spring_strength: float = 200;
@export var r_damper_strength: float = 10;
@export var r_wheel_radius: float = 0.3;
@export var r_wheel_mass: float = 0.02;
@export_range(0, 1, 0.05) var r_grip_factor: float = 0.8

@export_group("Steering", "steering_")
@export_range(0, 60, 0.01, "radians_as_degrees") var steering_range: float = deg_to_rad(30)
@export_range(0, 60, 0.01, "radians_as_degrees") var steering_hardness: float = deg_to_rad(5)

func _integrate_forces(state):
	if Input.is_action_pressed("brake"):
		state.linear_velocity = state.linear_velocity.move_toward(Vector3(0,0,0), state.step)
